<?php

    require_once 'usuario.class.php';

    $iduser=$_GET['iduser'];

?>
<html>
<head>
    <title>Cadastro Usuario</title>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<style type="text/css">
    
        body{
        background: url(img/vakinha1.jpg) no-repeat;
        background-size: 100%;
    }
    .novocadastro{
        margin-left:34.4%;
        border-top-right-radius: 5px;
        border-top-left-radius: 5px;
        background:#e67e22; 
        margin-right: 34.3%;
        margin-top:5%; 
        font-size: 15px;
        text-align: center;
        padding-left: 50px;
        padding-right:  50px;
        padding-top: 20px;
        padding-bottom: 20px;
        color:#fff;
        font-weight: bold;
        font-family: fonts/glyphicons-halflings-regular.ttf;

    }
    #bg-opacity{
        background:#fff;
        margin-right:33%;
        margin-left:33%;
        padding-top: 345px;
        padding-left: 70px; 
        padding-right: 70px;
        padding-bottom: 345px;
        border-radius: 10px;
        border: 15px;
        opacity: 0.3;
        border-color: #fff;
    }
    #bg-cadastro{
        background:#fff;
        padding-top: 50px;
        padding-left: 50px; 
        padding-right: 50px;
        padding-bottom: 30px;
        border-bottom-right-radius: 10px;
        border-bottom-left-radius: 10px;
        border: 15px;
        border-color: #fff;
    }
    #btn-signup{
        background: #e67e22;
        margin-left: -150%;
        margin-right: 150%;
        padding-left: 230%; 
        padding-right: 260%;
        padding-bottom: 10px; 
        padding-top: 10px; 
        border:0px;
        border-radius: 0px;
    }

</style>
<body>

<!-- MENU - NAVBAR PRINCIPAL -->
<nav class="navbar navbar-inverse" style="background:#e67e22">
  <div class="container-fluid" style="background:#e67e22" >
    <div class="navbar-header" style="background:#e67e22" >
    <ul class="nav navbar-nav navbar-right" style="background:#e67e22" >
      <li><a href="index.php" style="color:#fff" ><span class="glyphicon glyphicon-log-in" style="color:#fff" ></span> Voltar</a></li>
    </ul>
  </div>
</nav>


<!-- FORMULARIO DE CADASTRO DE VAKKINHA -->
<div  class="novocadastro">NOVA VAKKINHA</div>
<div class="col-md-4 col-md-offset-4" style="margin-top:0px;">
<div id="bg-cadastro">
                                <form id="signupform" class="form-horizontal" role="form" class="col-md-6 col-md-offset-3" action="inserirvakkinha.php" method="post">
                                    <div id="signupalert" style="display:none; " class="alert alert-danger">
                                        <p>Error:</p>
                                        <span></span>
                                    </div>

                                    <div style="margin-bottom: 25px;" class="input-group">

                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                        <input type="text" class="form-control" name="titulo" value="" placeholder="Titulo da Vakkinha">
                                        <input type="text" class="form-control" name="finalidade" value="" placeholder="Finalidade">
                                        <input type="text" class="form-control" name="meta" value="" placeholder="Meta">
                                        <input type="date" class="form-control" name="encerra_em" value="" placeholder="Data de Encerramento">
                                        <input type="text" class="form-control" name="descricao" value="" placeholder="Descrição">
                                        <input type="text" class="form-control" name="linkimg" value="" placeholder="Link da Imagem">
                                        <input type="text" class="form-control" name="linkvideo" value="" placeholder="Link do Video">
                                        <?php
                                       print "<input type='hidden' class='form-control' name='iduser' placeholder='Código do Usuário' 
                                        value='".$iduser."'>"; ?>
                                    </div>

                                    <div class="form-group" style="">
                                        <div class="col-lg-offset-3 col-lg-3" >
                                            <button id="btn-signup" type="submit" class="btn btn-danger" ><i class="icon-hand-right" style="font-weight: bold;" ></i>Criar</button>
                                        </div>
                                    </div>
                                </form>
                                </div>                         
                                       </div>
</body>
</html>