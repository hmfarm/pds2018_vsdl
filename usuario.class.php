<?php
	require_once 'conexao.class.php';
	class Usuario{
		
		private $iduser;
		private $nome;
		private $CPF;
		private $email;
		private $senha;
		private $dtnascimento;
		private $telefone;
		private $estado;
		private $cidade;
		
		public function getIduser(){
			return $this->iduser;
		}
		public function setIduser($iduser){
			if(!empty($iduser)) $this->iduser=$iduser;
		}
		public function getNome(){
			return $this->nome;
		}
		public function setNome($nome){
			if(!empty($nome)) $this->nome=$nome;
		}
		public function getCPF(){
			return $this->CPF;
		}
		public function setCPF($CPF){
			if(!empty($CPF)) $this->CPF=$CPF;
		}
		public function getEmail(){
			return $this->email;
		}
		public function setEmail($email){
			if(!empty($email)) $this->email=$email;
		}
		public function getSenha(){
			return $this->senha;
		}
		public function setSenha($senha){
			if(!empty($senha)) $this->senha=$senha;
		}
				public function gettelefone(){
			return $this->telefone;
		}
		public function settelefone($telefone){
			if(!empty($telefone)) $this->telefone=$telefone;
		}
				public function getdtnascimento(){
			return $this->dtnascimento;
		}
		public function setdtnascimento($dtnascimento){
			if(!empty($dtnascimento)) $this->dtnascimento=$dtnascimento;
		}
		public function getEstado(){
			return $this->estado;
		}
		public function setEstado($estado){
			if(!empty($estado)) $this->estado=$estado;
		}

		public function getCidade(){
			return $this->cidade;
		}
		public function setCidade($cidade){
			if(!empty($cidade)) $this->cidade=$cidade;
		}

		public function inserir(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare(
				"INSERT INTO usuario(nome,CPF,email,senha,dtnascimento,telefone,estado,cidade)
				VALUES(:nome,:CPF,:email,:senha,:dtnascimento,:telefone,:estado,:cidade)");
				$stmt->bindValue(":nome",$this->getNome());
				$stmt->bindValue(":CPF",$this->getCPF());
				$stmt->bindValue(":email",$this->getEmail());
				$stmt->bindValue(":senha",$this->getSenha());
				$stmt->bindValue(":telefone",$this->gettelefone());
				$stmt->bindValue(":dtnascimento",$this->getdtnascimento());
				$stmt->bindValue(":estado",$this->getEstado());
				$stmt->bindValue(":cidade",$this->getCidade());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function alterar(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare(
				"UPDATE usuario SET iduser=:iduser, nome=:nome, CPF=:CPF, email=:email,senha=:senha,dtnascimento=:dtnascimento,telefone=:telefone,
				estado=:estado,cidade=:cidade where iduser=:iduser");
				$stmt->bindValue(':iduser',$this->getIduser());
				$stmt->bindValue(":nome",$this->getNome());
				$stmt->bindValue(":CPF",$this->getCPF());
				$stmt->bindValue(":email",$this->getEmail());
				$stmt->bindValue(":senha",$this->getSenha());
				$stmt->bindValue(":telefone",$this->gettelefone());
				$stmt->bindValue(":dtnascimento",$this->getdtnascimento());
				$stmt->bindValue(":estado",$this->getEstado());
				$stmt->bindValue(":cidade",$this->getCidade());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

	public function Login(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare("SELECT * from usuario where email=:email and senha=:senha");
				$stmt->bindValue(':email',$this->getEmail());
				$stmt->bindValue(':senha',$this->getSenha());
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
				}
			}

			public function buscarTodos(){
			$conect = new conexao();
			try {
				$stmt = $conect->conn->prepare("select * from usuario");
				$stmt->execute();
				$r=$stmt->fetchAll();
				$resposta = array();
				foreach ($r as $row) {
					$temp=array("iduser" =>$row['iduser'], 
						"nome"=>$row['nome'],
						"CPF"=>$row['CPF'],
						"email"=>$row['email'],
						"senha"=>$row['senha']);
					array_push($resposta, $temp);
				}
				return $resposta;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarId(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare(
				"select * from usuario where iduser=:iduser");
				$stmt->bindValue(':iduser',$this->getIduser());
				$stmt->execute();
				$r=$stmt->fetch();
				
				return $r;

			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		
}

?>