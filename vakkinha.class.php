<?php
	require_once 'conexao.class.php';

	class Vakkinha{
		
		private $idvakkinha;
		private $titulo;
		private $finalidade;
		private $meta;
		private $encerra_em;
		private $descricao;
		private $linkimg;
		private $linkvideo;
		private $iduser;
		
		public function getuser(){
			return $this->iduser;
		}
		public function setuser($iduser){
			if(!empty($iduser)) $this->iduser=$iduser;
		}
		public function getidvakkinha(){
			return $this->idvakkinha;
		}
		public function setidvakkinha($idvakkinha){
			if(!empty($idvakkinha)) $this->idvakkinha=$idvakkinha;
		}
		public function gettitulo(){
			return $this->titulo;
		}
		public function settitulo($titulo){
			if(!empty($titulo)) $this->titulo=$titulo;
		}
		public function getfinalidade(){
			return $this->finalidade;
		}
		public function setfinalidade($finalidade){
			if(!empty($finalidade)) $this->finalidade=$finalidade;
		}
		public function getmeta(){
			return $this->meta;
		}
		public function setmeta($meta){
			if(!empty($meta)) $this->meta=$meta;
		}
		public function getencerra_em(){
			return $this->encerra_em;
		}
		public function setencerra_em($encerra_em){
			if(!empty($encerra_em)) $this->encerra_em=$encerra_em;
		}
				public function getlinkimg(){
			return $this->linkimg;
		}
		public function setlinkimg($linkimg){
			if(!empty($linkimg)) $this->linkimg=$linkimg;
		}
				public function getdescricao(){
			return $this->descricao;
		}
		public function setdescricao($descricao){
			if(!empty($descricao)) $this->descricao=$descricao;
		}
		public function getlinkvideo(){
			return $this->linkvideo;
		}
		public function setlinkvideo($linkvideo){
			if(!empty($linkvideo)) $this->linkvideo=$linkvideo;
		}

		public function inserir(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare(
				"INSERT INTO vakkinha(titulo,finalidade,meta,encerra_em,descricao,linkimg,linkvideo,iduser)
				VALUES(:titulo,:finalidade,:meta,:encerra_em,:descricao,:linkimg,:linkvideo,:iduser)");
				$stmt->bindValue(":titulo",$this->gettitulo());
				$stmt->bindValue(":finalidade",$this->getfinalidade());
				$stmt->bindValue(":meta",$this->getmeta());
				$stmt->bindValue(":encerra_em",$this->getencerra_em());
				$stmt->bindValue(":linkimg",$this->getlinkimg());
				$stmt->bindValue(":descricao",$this->getdescricao());
				$stmt->bindValue(":linkvideo",$this->getlinkvideo());
				$stmt->bindValue(":iduser",$this->getuser());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function alterar(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare(
				"UPDATE vakkinha SET idvakkinha=:idvakkinha, titulo=:titulo,finalidade=:finalidade,meta=:meta,encerra_em=:encerra_em, descricao=:descricao,linkimg=:linkimg,linkvideo=:linkvideo,iduser=:iduser WHERE idvakkinha=:idvakkinha");
				$stmt->bindValue(":idvakkinha",$this->getidvakkinha());
				$stmt->bindValue(":titulo",$this->gettitulo());
				$stmt->bindValue(":finalidade",$this->getfinalidade());
				$stmt->bindValue(":meta",$this->getmeta());
				$stmt->bindValue(":encerra_em",$this->getencerra_em());
				$stmt->bindValue(":linkimg",$this->getlinkimg());
				$stmt->bindValue(":descricao",$this->getdescricao());
				$stmt->bindValue(":linkvideo",$this->getlinkvideo());
				$stmt->bindValue(":iduser",$this->getuser());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

			public function delete(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare(
					"DELETE from vakkinha WHERE idvakkinha=:idvakkinha");
				$stmt->bindValue(":idvakkinha",$this->getidvakkinha());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarTodos(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare("SELECT * from vakkinha order by idvakkinha");
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}

		public function buscarID(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare("SELECT * from vakkinha WHERE idvakkinha=:idvakkinha order by idvakkinha");
				$stmt->bindValue(':idvakkinha',$this->getidvakkinha());
				$stmt->execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
				}
			}	
		

		public function buscarIDUser(){
			$conect = new conexao();
			try{
				$stmt = $conect->conn->prepare("SELECT * from vakkinha WHERE iduser=:iduser order by idvakkinha");
				$stmt->bindValue(':iduser',$this->getuser());
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
				}
			}	
		}
		
		

?>