<?php
    session_start();

    if (!isset($_SESSION['iduser']))    {

            header("location:index.html");
        }

    require_once 'usuario.class.php';
    require_once 'vakkinha.class.php';

    $usuario = new usuario();
    $vakkinha2 = new vakkinha();
    $usuario->setIduser($_SESSION['iduser']);
    $r = $usuario->buscarId();
    $idresp = $usuario->getIduser();

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8"/>
	<title>Bem vindo</title>
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
    <script type="text/javascript" src="js/jquery-3.1.1.js"></script>
	<script type="text/javascript" src="js/menuUsuario.js"></script>
</head>

 <style type="text/css">
     body{
         background: #ffdead;
     }
     .nav a:hover {
        color: black;
        text-decoration: underline;
     }
     #img{

        width: 200px;
        margin-top: -20px;
        margin-left: -200px;

     }
     #user{
        margin-left: 10%;
        margin-top: 5%;
     }
 </style>

<body>

<nav class="navbar navbar-inverse"  style="background: rgb(139, 125, 123); border: 0">
  <div class="container-fluid" style="background: rgb(139, 125, 123); margin-left: 400px">
    <div class="navbar-header" style="background: rgb(139, 125, 123)">
      <a  class="navbar-brand" href="index.php" >
      <img src="img/logoborda.png" id="img"> </a>
    </div>
    <ul  id="lista-menu" class="nav navbar-nav" style="background:rgb(139, 125, 123)">
      <li><a href="index.php" style="color: #fff">Home</a></li>
      <li style="color: #fff">
        <?php 
        print "<a href='cadastrovakkinha.php?iduser=".$idresp."'style='color: #fff'>Crie sua Vakkinha</a>";?>
        </li>
</nav> 

<div id="wrapper">
        <div class="overlay"></div>
        <nav class="navbar navbar-inverse navbar-fixed-top" style="top: 10%;" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                       Usuário
                    </a>
                </li>
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Perfil</a>
                </li>
                <li>
                    <a href="#">Setup</a>
                </li>
                <li>
                    <a href="#">Ajuda</a>
                </li>
                <li class="dropdown">
                  <a href="#">Siga-nos</a>
                </li>
                <li>
                    <a href="index.html">Sair</a>

                </li>
            </ul>
        </nav>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
                <span class="hamb-middle"></span>
                <span class="hamb-bottom"></span>
            </button>
            <div class="container">
                <div class="row" id="user">
                    <div class="col-lg-8 col-lg-offset-2">
            <img src="imagens/user.png" class="img-user"/>

            <div  class="btn" type="submit" style="text-decoration:none">
            <?php
            
            print "<a href='alteruser.php?iduser=".$idresp."'style='color: #fff'>Editar Usuario</a>";
            ?>
          </div>

<div class="nome">
    <?php 
        echo $r['nome'];
    ?>
</div>

<?php
    $vakkinha2->setuser($idresp);
    $teste=$vakkinha2->getuser();
    $rest = $vakkinha2->buscarIDUser();

if(count($rest) > 0){
  
?>

<section class="table container"><b>
  <meta charset="utf-8"><h3 font face="Arial">Minhas Vakkinhas</h3><b>
  <table class="table table-hover">
    <tr class="warning">
      <th>id</th>
      <th>titulo</th>
      <th>finalidade</th>
      <th>meta</th>
      <th>encerra_em</th>
      <th>descricao</th>
      <th>linkimg</th>
      <th>linkvideo</th>
      <th>nº do usuário</th>
       <th>Alterar</th>
      <th>Excluir </th>
    </tr>

<?php

 foreach ($rest as $linha) {
    print "<tr>";
    print "<td>".$linha['idvakkinha']."</td>";
    print "<td>".$linha['titulo']."</td>";
    print "<td>".$linha['finalidade']."</td>";
    print "<td>".$linha['meta']."</td>";
    print "<td>".$linha['encerra_em']."</td>";
    print "<td>".$linha['descricao']."</td>";
    print "<td>".$linha['linkimg']."</td>";
    print "<td>".$linha['linkvideo']."</td>";
    print "<td>".$linha['iduser']."</td>";
    print "<td><a href='altervakkinha.php?idvakkinha=".$linha['idvakkinha']."'>Alterar</a><td>";
    print "<td><a href='apgvakkinha.php?idvakkinha=".$linha['idvakkinha']."'>Excluir</a><td>";
    print "</tr>";
  }
  print "</table>";
    }else{
        echo "Nenhuma Vaquinha Criada";
    }

?>
    </section>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
    </div>
    <!-- /#wrapper -->



</body>

</html>